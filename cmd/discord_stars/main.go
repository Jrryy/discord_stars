package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	dgo "github.com/bwmarrin/discordgo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"syscall"
	"time"
)

var discordToken string
var brawlStarsAPIToken string
var apiClient *http.Client
var mongoClient *mongo.Client

func addApiHeaders(request *http.Request) {
	request.Header.Add("Accept", "application/json")
	request.Header.Add("authorization", "Bearer "+brawlStarsAPIToken)
}

func getDatabase() *mongo.Database {
	return mongoClient.Database("db")
}

func getPlayerDataDict(response *http.Response) (data map[string]interface{}, e error) {
	defer response.Body.Close()
	body, e := ioutil.ReadAll(response.Body)
	if e != nil {
		return
	}
	e = json.Unmarshal(body, &data)
	return
}

func sendHelp(session *dgo.Session, channel string) error {
	helpString := "```\n" +
		"Usage of this bot (all commands are preceded by \";\"):\n" +
		"\t- ;h[elp]: Display this message.\n" +
		"\t- ;info <player tag>: Display data about a player with tag <player tag>. If no tag is provided, or there isn't any player with it, no data will be shown." +
		"```"
	_, e := session.ChannelMessageSend(channel, helpString)
	return e
}

func registerPlayer(session *dgo.Session, channel string, playerId string, discordId string) (e error) {
	db := getDatabase()
	collection := db.Collection("playerData")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	_, e = collection.FindOne(ctx, bson.M{"discordId": discordId}).DecodeBytes()
	if e != nil {
		playerUrl := url.PathEscape(playerId)
		request, e := http.NewRequest("GET", "https://api.brawlstars.com/v1/players/"+playerUrl, nil)
		if e != nil {
			return e
		}
		addApiHeaders(request)
		response, e := apiClient.Do(request)
		if e != nil {
			return e
		}
		if response.StatusCode != 200 {
			_, e = session.ChannelMessageSend(channel, "Player "+playerId+" does not exist.")
			return fmt.Errorf("player with id %s not found", playerId)
		}
		_, e = session.ChannelMessageSend(channel,
			"Player "+playerId+" not found. Creating entry in db...")
		if e != nil {
			return e
		}
		playerData, e := getPlayerDataDict(response)
		if e != nil {
			return e
		}
		_, e = collection.InsertOne(ctx,
			bson.M{"playerId": playerId,
				"discordId":     discordId,
				"name":          playerData["name"],
				"trophies":      playerData["trophies"],
				"3vs3Victories": playerData["3vs3Victories"]})
		if e != nil {
			return e
		}
	} else {
		_, e = session.ChannelMessageSend(
			channel,
			"You have already registered a player. If you want to unregister it, use \";unregister\".")
	}
	return
}

func sendPlayerData(session *dgo.Session, channel string, data map[string]interface{}) (e error) {
	playerDataString := fmt.Sprintf(
		"```\n"+
			"Player name: %v\n"+
			"Trophies: %v\n"+
			"Victories: %v\n"+
			"```",
		data["name"].(string),
		data["trophies"].(float64),
		data["3vs3Victories"].(float64))
	_, e = session.ChannelMessageSend(channel, playerDataString)
	return
}

func showPlayerData(session *dgo.Session, channel string, player string, discordId string) error {
	var playerDataDict map[string]interface{}
	if player != "" {
		playerUrl := url.PathEscape(player)
		request, e := http.NewRequest("GET", "https://api.brawlstars.com/v1/players/"+playerUrl, nil)
		if e != nil {
			return e
		}
		addApiHeaders(request)
		response, e := apiClient.Do(request)
		if e != nil {
			return e
		}
		if response.StatusCode != 200 {
			return fmt.Errorf("player with id %s not found", player)
		}
		defer response.Body.Close()
		body, e := ioutil.ReadAll(response.Body)
		if e != nil {
			return e
		}
		e = json.Unmarshal(body, &playerDataDict)
		if e != nil {
			return e
		}
	} else {
		db := getDatabase()
		collection := db.Collection("playerData")
		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		result, e := collection.FindOne(ctx, bson.M{"discordId": discordId}).DecodeBytes()
		e = bson.Unmarshal(result, &playerDataDict)
		if e != nil {
			return e
		}
	}
	e := sendPlayerData(session, channel, playerDataDict)
	return e
}

func messageHandler(session *dgo.Session, m *dgo.MessageCreate) {
	if m.Author.ID == session.State.User.ID {
		return
	}
	re, _ := regexp.Compile(";(\\w+).*")
	var e error
	matchedCommand := re.FindStringSubmatch(m.Content)
	if len(matchedCommand) > 1 {
		switch matchedCommand[1] {
		case "h", "help":
			e = sendHelp(session, m.ChannelID)
		case "r", "register":
			matchedLastIndex := re.FindStringSubmatchIndex(m.Content)[3]
			playerId := strings.TrimSpace(m.Content[matchedLastIndex:])
			e = registerPlayer(session, m.ChannelID, playerId, m.Author.ID)
		case "info":
			matchedLastIndex := re.FindStringSubmatchIndex(m.Content)[3]
			playerId := strings.TrimSpace(m.Content[matchedLastIndex:])
			e = showPlayerData(session, m.ChannelID, playerId, m.Author.ID)
		default:
			log.Printf("The command %s was invalid ", m.Content)
		}
		if e != nil {
			log.Print(e)
		}
	}
}

func getTokens() (e error) {
	tokenFlag := flag.String("token", "", "Token found in https://discordapp.com/developers/applications/<bot_id>/bot")
	apiToken := flag.String("apiToken", "", "Token obtained in the Brawl Stars developers portal")
	flag.Parse()
	tokenEnv, foundToken := syscall.Getenv("DISCORD_STARS_TOKEN")
	apiTokenEnv, foundApiToken := syscall.Getenv("BRAWL_STARS_API_TOKEN")
	if !foundToken {
		if *tokenFlag == "" {
			e = fmt.Errorf("discord token was not found")
			flag.PrintDefaults()
		} else {
			discordToken = *tokenFlag
		}
	} else {
		discordToken = tokenEnv
	}
	if !foundApiToken {
		if *apiToken == "" {
			e = fmt.Errorf("brawl stars api token not found")
		} else {
			brawlStarsAPIToken = *apiToken
		}
	} else {
		brawlStarsAPIToken = apiTokenEnv
	}
	return
}

func testApi() (e error) {
	request, e := http.NewRequest("GET", "https://api.brawlstars.com/v1/players/%239UG88U0RJ", nil)
	if e != nil {
		return
	}
	addApiHeaders(request)
	response, e := apiClient.Do(request)
	if e != nil {
		return
	}
	if response.StatusCode != 200 {
		e = fmt.Errorf("the api returned status code %v", response.StatusCode)
	}
	return
}

func testMongo() (e error) {
	return
}

func main() {
	e := getTokens()
	if e != nil {
		fmt.Println("An error occurred when obtaining the tokens: ", e)
		return
	}
	session, e := dgo.New("Bot " + discordToken)
	if e != nil {
		fmt.Println("An error occurred when opening a connection to Discord: ", e)
		return
	}
	apiClient = &http.Client{}
	e = testApi()
	if e != nil {
		fmt.Println("An error occurred when testing the API: ", e)
		return
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	mongoClient, e = mongo.Connect(ctx, options.Client().ApplyURI("mongodb://db:27017"))
	if e != nil {
		fmt.Println("An error occurred when connecting to mongodb: ", e)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	session.AddHandler(messageHandler)

	// Open a websocket connection to Discord and begin listening.
	e = session.Open()
	if e != nil {
		fmt.Println("error opening connection, ", e)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	e = session.Close()
	if e != nil {
		fmt.Println("An error occurred when closing the Discord session: ", e)
	}
}
